# Synopsys Template

Synopsys Template allows you to configure your GitLab pipeline to run Synopsys security testing and take action on the results. Synopsys Template leverages Synopsys Bridge, which allows you to run tests for several Synopsys products from the command line. The latest version of Synopsys Bridge is available [[here](https://sig-repo.synopsys.com/artifactory/bds-integrations-release/com/synopsys/integration/synopsys-action/)].

# Quick Start for the Synopsys Template

Synopsys Template supports the following Synopsys security testing solutions:
- Polaris, a SaaS-based solution offering SAST, SCA and Managed Services in a single unified platform
- Coverity, using our thin client and cloud-based deployment model
- Black Duck Hub, supporting either on-premises or hosted instances

**Note:** Synopsys Template requires appropriate licenses for all Synopsys application used.

**To run Synopsys Template:**
- Validate Scanning platform-related parameters like project and stream.
- Download Synopsys Bridge and related adapters.
- Run corresponding Synopsys security testing solutions (Polaris, Coverity, or Black Duck). 

Synopsys solution functionality is invoked directly by Synopsys Bridge, and indirectly by the Synopsys Template, which downloads Synopsys Bridge and calls the respective adapters to run corresponding scans.

## Synopsys Template - Polaris

Before running a pipeline using the Synopsys Template and Polaris, add .gitlab-ci.yml to your project by adding an include entry.

Configure sensitive data like access tokens and URLs using GitLab secrets.

Push those changes and GitLab runner will pickup the job and initiate the pipeline.

```yaml
include:
  - project: synopsys/synopsys-template
    ref: main
    file: templates/synopsys-template.yml
variables:
  BRIDGE_POLARIS_SERVERURL: ${POLARIS_SERVER_URL}
  BRIDGE_POLARIS_ACCESSTOKEN: ${POLARIS_ACCESS_TOKEN}
  BRIDGE_POLARIS_APPLICATION_NAME: 'testapp1'
  BRIDGE_POLARIS_PROJECT_NAME: 'testproj1'
  # Accepts Multiple Values.
  BRIDGE_POLARIS_ASSESSMENT_TYPES: 'SCA,SAST'      
stages:
  - template_execution
synopsys_template_execution:
  stage: template_execution
  tags: 
    - macos-runner
  extends: .run-synopsys-tools # used for bash
  #extends: .run-synopsys-tools-powershell # used for powershell
```
# Synopsys Template - Coverity Cloud Deployment with Thin Client

**Note:** At this time, Synopsys Template only supports the Coverity thin client/cloud deployment model, which removes the need for a large footprint GitLab Runner installation.

Before running Coverity using the Synopsys Template, ensure the appropriate project and stream are set in your Coverity Connect server environment.

Configure sensitive data like usernames, passwords and URLs using GitLab secrets.

```yaml

include:
  - project: synopsys/synopsys-template
    ref: main
    file: templates/synopsys-template.yml
variables:
  BRIDGE_COVERITY_CONNECT_URL: ${COVERITY_URL}
  BRIDGE_COVERITY_CONNECT_USER_NAME: ${COVERITY_USER}
  BRIDGE_COVERITY_CONNECT_USER_PASSWORD: ${COVERITY_PASSWORD}
  BRIDGE_COVERITY_CONNECT_PROJECT_NAME: 'coverity-project-name'
  BRIDGE_COVERITY_CONNECT_STREAM_NAME: 'coverity-stream-name'      
stages:
  - template_execution
synopsys_template_execution:
  stage: template_execution
  tags: 
    - macos-runner
  extends: .run-synopsys-tools # used for bash.        
  #extends: .run-synopsys-tools-powershell #used for powershell
```
**Optional Parameters:** 
- BRIDGE_COVERITY_CONNECT_POLICY_VIEW, 
- BRIDGE_COVERITY_INSTALL_DIRECTORY

## Synopsys Template - Black Duck 

Synopsys Template supports both self-hosted (on-prem) and Synopsys-hosted Black Duck Hub instances.

In the default Black Duck Hub permission model, projects and project versions are created on the fly as needed.

Configure sensitive data like access tokens and URLs using GitLab secrets.

```yaml
include:
  - project: synopsys/synopsys-template
    ref: main
    file: templates/synopsys-template.yml
variables:
  BRIDGE_BLACKDUCK_URL: ${BLACKDUCK_URL}
  BRIDGE_BLACKDUCK_TOKEN: ${BLACKDUCK_API_TOKEN}      
stages:
  - template_execution
synopsys_template_execution:
  stage: template_execution
  tags: 
    - macos-runner
  extends: .run-synopsys-tools # used for bash.        
  #extends: .run-synopsys-tools-powershell #used for powershell
```
**Optional Parameters:** 
- BRIDGE_BLACKDUCK_SCAN_FULL, 
- BRIDGE_BLACKDUCK_INSTALL_DIRECTORY, 
- BRIDGE_BLACKDUCK_SCAN_FAILURE_SEVERITIES
- **Note about Detect command line parameters**: Any command line parameters that you need to pass to detect can be passed through variables. This is a standard capability of Detect. For example, if you want to only report newly found policy violations on rapid scans, you would normally use the command line --detect.blackduck.rapid.compare.mode=BOM_COMPARE_STRICT. You can replace this by setting the DETECT_BLACKDUCK_RAPID_COMPARE_MODE variable to BOM_COMPARE_STRICT.

## Additional Parameters:
  
- **DOWNLOAD_BRIDGE_URL :** If provided, Synopsys Bridge is automatically downloaded and configured in the GitLab runner's repository.

- **DOWNLOAD_BRIDGE_VERSION :** If provided, the specified version of Synopsys Bridge is automatically downloaded and configured in the GitLab runner's repository.

**Note:**

- If **DOWNLOAD_BRIDGE_VERSION** or **DOWNLOAD_BRIDGE_URL** is not provided, Synopsys Template automatically downloads and configures the latest version of Synopsys Bridge.

- As per current behavior, the existing directory will be cleaned and then Synopsys Template automatically downloads and configures Synopsys Bridge every time.

- **tags** refer to GitLab runner's tag name.

# Gitlab Runner Setup

- GitLab Runner can be installed and used on GNU/Linux, macOS and Windows. Refer the given documentation : 
https://docs.gitlab.com/runner/

- During runner registration, choose executor as Shell.

- After setting up the GitLab runner, make sure you have unzip package tools installed in your system (Linux/Mac).

  Note: Synopsys Template supports both Project runners and Shared runners (except Shared Mac Runners).

